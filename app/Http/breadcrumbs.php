<?php

Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('<img src="'.asset("/images/home-ico.png").'" alt="Home"> Home', route('home'));
});

Breadcrumbs::register('projects.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Projects', route('projects.index'));
});

Breadcrumbs::register('projects.show', function ($breadcrumbs, $slug) {
    $breadcrumbs->parent('projects.index');
    $breadcrumbs->push('Videos', route('projects.show', $slug));
});

Breadcrumbs::register('projects.videos.show', function ($breadcrumbs, $project_slug, $slug) {
    $videoRepository = App::make('App\Repositories\VideoRepository');
    $video = $videoRepository->findBySlug($slug)->first(['title']);
    $breadcrumbs->parent('projects.show', $project_slug);
    $breadcrumbs->push($video->title, route('projects.videos.show', $slug));
});

Breadcrumbs::register('account.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Account', route('account.index'));
});

Breadcrumbs::register('account.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('account.index');
    $breadcrumbs->push('Edit Account', route('account.edit'));
});

Breadcrumbs::register('usage_stats.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Usage Stats', route('usage_stats.index'));
});

Breadcrumbs::register('usage_stats.show', function ($breadcrumbs, $project_slug) {
    $projectRepository = App::make('App\Repositories\ProjectRepository');
    $project = $projectRepository->findBySlug($project_slug)->first(['title']);
    $breadcrumbs->parent('usage_stats.index');
    $breadcrumbs->push($project->title, route('usage_stats.show', $project_slug));
});

Breadcrumbs::register('audiences.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Audiences', route('audiences.index'));
});

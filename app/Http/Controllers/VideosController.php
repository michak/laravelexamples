<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\VideoService;
use App\Repositories\VideoRepository;
use App\Repositories\ProjectRepository;

use DB;
use Flash;

class VideosController extends Controller
{

    public function __construct(
        VideoRepository $videoRepository,
        VideoService $videoService,
        ProjectRepository $projectRepository
    ) {
        $this->videoRepository = $videoRepository;
        $this->projectRepository = $projectRepository;
        $this->videoService = $videoService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($slug)
    {
        return redirect(route('projects.show', $slug));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store($project_slug, Request $request)
    {
        $validator = $this->videoService->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request,
                $validator
            );
        }

        if ($project = $this->projectRepository->findBySlug($project_slug)->firstOrFail()) {
            if ($videoData = $this->videoService->upload($request)) {
                $video = $this->videoRepository->create($request->all(), $project);
                $video->filename = $videoData['filename'];
                $video->filesize = $videoData['filesize'];
                $video->job_id = $videoData['job_id'];
                $video->save();

                if ($project->videos()->save($video)) {
                    Flash::message('Video has been created successfully');

                    return redirect(route('projects.show', $project_slug));
                }
            }
        }

        Flash::message('Video couldn\'t be created');
        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($project, $slug)
    {
        $video = $this->videoRepository->findBySlug($slug)->with('settings', 'videoPlays', 'callActionImage', 'callActionText', 'callActionHtml', 'pixelbombFacebook', 'pixelbombGoogle', 'pixelbombCustom')->firstOrFail();
        $videoPlays = $video->videoPlays()->select(DB::raw('*, count(*) as duration_count'))
            ->groupBy('duration')
            ->get();
        $userPlays = $video->videoPlays()->groupBy('ip_address')->get();
        $sales = $video->sales()->get();

        if ($video) {
            return view('videos.show')->with('video', $video)
                                        ->with('videoPlays', $videoPlays)
                                        ->with('userPlays', $userPlays)
                                        ->with('sales', $sales);
        } else {
            abort('404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($project_slug, $slug)
    {
        if ($video = $this->videoRepository->findBySlug($slug, true)->firstOrFail()) {
            $video_copies = $this->videoRepository->where(array('filename' => $video->filename));

            if ($video_copies->count() == 1) {
                $this->videoService->remove($video);
            }

            if ($this->videoRepository->delete($video->id)) {
                return redirect(route('projects.show', $project_slug));
            }
        }

        abort('401');
    }

    /**
     * Update video settings for the specified resource in storage
     *
     * @param string $slug
     * @param Request $request
     * @return Response
     */
    public function updateSettings($project_slug, $slug, Request $request)
    {
        $video = $this->videoRepository->findBySlug($slug)->with('settings')->firstOrFail();
        $videoSettings = $this->videoRepository->updateSettings($video, $request->all());

        if ($videoSettings) {
            return redirect(route('projects.videos.show', [$project_slug, $slug]));
        } else {
            abort('401');
        }
    }

    /**
     * Update video "call to action" for the specified resource in storage
     *
     * @param string $slug
     * @param Request $request
     * @return Response
     */
    public function updateCallAction($project_slug, $slug, Request $request)
    {
        $validator = $this->videoService->callActionValidator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request,
                $validator
            );
        }

        $video = $this->videoRepository->findBySlug($slug)->with('callActionHtml', 'callActionText', 'callActionImage')->firstOrFail();
        $videoCallAction = $this->videoRepository->updateCallAction($video, $request);

        if ($videoCallAction) {
            return redirect(route('projects.videos.show', [$project_slug, $slug]));
        } else {
            abort('401');
        }
    }

    public function embed($slug)
    {
        $video = $this->videoRepository->findBySlug($slug)->with('settings', 'callAction', 'pixelbombs')->firstOrFail();

        if ($video) {
            return view('videos.embed')->with('video', $video);
        } else {
            abort('404');
        }
    }

    public function replicate($project_slug, $slug)
    {
        if ($video = $this->videoRepository->findBySlug($slug, true)->firstOrFail()) {
            if ($this->videoRepository->replicate($video)) {
                return redirect(route('projects.show', $project_slug));
            }
        }

        abort('401');
    }

    /**
     * Update video thumbnail for the specified resource in storage
     *
     * @param string $slug
     * @param Request $request
     * @return Response
     */
    public function updateThumbnail($project_slug, $slug, Request $request)
    {
        $validator = $this->videoService->thumbnailValidator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request,
                $validator
            );
        }

        $video = $this->videoRepository->findBySlug($slug)->firstOrFail();

        $thumbnail = $this->videoService->uploadThumbnail($video, $request);
        
        if ($thumbnail) {
            $videoThumbnail = $this->videoRepository->updateThumbnail($video, $thumbnail);
        } else {
            $videoThumbnail = $this->videoRepository->updateThumbnail($video, $request->input('thumbnail'));
        }

        if ($videoThumbnail) {
            return redirect(route('projects.videos.show', [$project_slug, $slug]));
        } else {
            abort('401');
        }
    }
}

<?php namespace App\Http\Controllers\Auth;

use Mail;
use Auth;
use Hash;
use Flash;
use Setting;
use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $username = 'username';
    protected $redirectPath = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(UserService $userService, UserRepository $userRepository)
    {
        $this->userService = $userService;
        $this->userRepository = $userRepository;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name'     => $data['name'],
            'username' => $data['username'],
            'email'    => $data['email'],
            'password' => $data['password'],
        ]);

        $user->activation_token = str_random(40);
        $user->save();

        return $user;
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('auth.login');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->userService->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request,
                $validator
            );
        }

        $user = $this->create($request->all());

        Mail::send('emails.activation', ['user' => $user], function ($m) use ($user) {
            $m->from('no-reply@wiseoplay.com', 'Wiseoplay.com');
            $m->to($user->email, $user->name)->subject(
                Setting::get('app.title', 'Wiseoplay') . ' - account confirmation'
            );
        });

        Flash::info('Please check your email account to confirm your account.');

        return redirect(route('auth.confirm'));
    }

    public function getActivate($activation_token)
    {
        $user = $this->userRepository->findByActivationToken($activation_token)->first();

        $user->active = true;
        $user->activation_token = null;
        $user->save();

        Auth::login($user);

        Flash::success('Welcome on board! Your account has been activated.');

        return redirect($this->redirectPath());
    }

    public function getConfirm()
    {
        return view('auth.confirm');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'auth'], function () {
    /**
     * Home route
     */
    Route::get('/', array('as' => 'home', 'uses' => 'ProjectsController@index'));

    /**
     * Projects routes
     */
    Route::resource('projects', 'ProjectsController');
    Route::get('projects/{project_slug}/replicate', array('as' => 'projects.replicate', 'uses' => 'ProjectsController@replicate'));

    /**
     * Videos routes
     */
    Route::resource('projects.videos', 'VideosController');
    Route::put('projects/{project_slug}/videos/{video_slug}/update-settings', array('as' => 'projects.videos.update_settings', 'uses' => 'VideosController@updateSettings'));
    Route::put('projects/{project_slug}/videos/{video_slug}/update-callaction', array('as' => 'projects.videos.update_call_action', 'uses' => 'VideosController@updateCallAction'));
    Route::put('projects/{project_slug}/videos/{video_slug}/update-pixelbombs', array('as' => 'projects.videos.update_pixelbombs', 'uses' => 'PixelbombsController@update'));
    Route::put('projects/{project_slug}/videos/{video_slug}/update-thumbnail', array('as' => 'projects.videos.update_thumbnail', 'uses' => 'VideosController@updateThumbnail'));
    Route::get('projects/{project_slug}/videos/{video_slug}/replicate', array('as' => 'projects.videos.replicate', 'uses' => 'VideosController@replicate'));

    /**
     * User/Account routes
     */
    Route::get('account', array('as' => 'account.index', 'uses' => 'AccountController@show'));
    Route::get('account/edit', array('as' => 'account.edit', 'uses' => 'AccountController@edit'));
    Route::put('account', array('as' => 'account.update', 'uses' => 'AccountController@update'));

    /**
     * Usage Stats routes
     */
    Route::get('usage-stats/project/{project_slug}/{date_from?}/{date_to?}', ['as' => 'usage_stats.show', 'uses' => 'UsageStatsController@show']);
    Route::get('usage-stats/{date_from?}/{date_to?}', ['as' => 'usage_stats.index', 'uses' => 'UsageStatsController@index']);

    /**
     * Audiences routes
     */
    Route::resource('audiences', 'AudiencesController');
    Route::resource('audiences.audience_sets', 'AudienceSetsController');
});

/**
 * User login/logout routes
 */
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

/**
 * User registration routes
 */
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::get('auth/activate/{activation_token}', ['as' => 'auth.activate', 'uses' => 'Auth\AuthController@getActivate']);
Route::get('auth/confirm', ['as' => 'auth.confirm', 'uses' => 'Auth\AuthController@getConfirm']);

/**
 * Password remind routes
 */
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

/**
 * Password reset routes
 */
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

/**
 * Video embed route
 */
Route::get('embed/{job_id}', array('as' => 'videos.embed', 'uses' => 'VideosController@embed'));

/**
 * Public video plays routes
 */
Route::resource('videos.video_plays', 'VideoPlaysController');

/**
 * Store sales tracking
 */
Route::get('sales/{job_id}', array('as' => 'sales.create', 'uses' => 'SalesController@create'));
Route::get('sales/{job_id}/track', array('as' => 'sales.store', 'uses' => 'SalesController@store'));

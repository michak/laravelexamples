<?php namespace App\Repositories;

use Auth;
use Image;
use Illuminate\Support\Str;
use App\Abstracts\AbstractRepository as AbstractRepository;

use App\Models\VideoSetting;

class VideoRepository extends AbstractRepository implements RepositoryInterface
{
    protected $modelClassName = 'App\Models\Video';
    protected $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function allByProject($project, $columns = array('*'))
    {
        return $project->videos()->select($columns)->orderBy('created_at', 'DESC')->get();
    }

    public function paginate($project, $limit, $columns = array('*'))
    {
        return $project->videos()->select($columns)->orderBy('created_at', 'DESC')->paginate($limit);
    }

    public function create(array $attributes, $parent = null)
    {
        $video = $this->createInstance(array($attributes));
        $video->slug = $this->getSlug($attributes['title']);
        $video->user()->associate($this->user);
        $video = $parent->videos()->save($video);

        return $video;
    }

    public function getFileName($video)
    {
        $fileName = strip_tags($video->getClientOriginalName());

        $slug = date('U') . '-' . Str::slug($fileName);
        $results = call_user_func("{$this->modelClassName}::whereRaw", "filename REGEXP '^{$slug}(-[0-9]*)?$'");
        $slugCount = $results->count();
     
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

    public function updateSettings($video, $settings)
    {
        $videoSettings = $video->settings;

        if ($videoSettings->update($settings)) {
            return true;
        }

        return false;
    }

    public function findByJobId($job_id, $authorize = false)
    {
        if ($authorize) {
            return $this->user->videos()->where('job_id', $job_id);
        } else {
            return call_user_func_array("{$this->modelClassName}::where", array(array('job_id' => $job_id)));
        }
    }

    public function findBySlug($slug, $authorize = false)
    {
        if ($authorize) {
            return $this->user->videos()->where('slug', $slug);
        } else {
            return call_user_func_array("{$this->modelClassName}::where", array(array('slug' => $slug)));
        }
    }

    public function updateCallAction($video, $call_action)
    {
        $callActionImage = $video->callActionImage;
        $callActionHtml = $video->callActionHtml;
        $callActionText = $video->callActionText;

        foreach ($call_action->input('image') as $key => $value) {
            $callActionImage->$key = $value;
        }

        if ($call_action->hasFile('image.file')) {
            if ($call_action->file('image.file')->isValid()) {
                $imageFile = $call_action->file('image.file');
                $filename = $this->getFileName($imageFile);
                $image = Image::make($imageFile)
                    ->resize($video->settings->width, $video->settings->height, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save(config('aws.call_action_destination_path') . $filename . '.' . $imageFile->getClientOriginalExtension());
            }

            $callActionImage->filename = $filename . '.' . $imageFile->getClientOriginalExtension();
        }

        foreach ($call_action->get('html') as $key => $value) {
            $callActionHtml->$key = $value;
        }

        foreach ($call_action->get('text') as $key => $value) {
            $callActionText->$key = $value;
        }

        $callActionImage->save();
        $callActionHtml->save();
        $callActionText->save();

        return true;
    }

    public function updateThumbnail($video, $thumbnail)
    {
        $video->thumbnail = $thumbnail;

        return $video->save();
    }
}

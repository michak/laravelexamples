<?php namespace App\Helpers;

use App;
use AWS;

/**
* Helpers to easily get video informations among views
*/
class VideoHelper
{
    public static function isComplete($video)
    {
        $transcoder = AWS::createClient('elastictranscoder');

        $job = $transcoder->readJob(array(
            'Id'    => $video->job_id
        ));

        if ($job['Job']['Status'] == 'Complete') {
            $video->status = 'Complete';
            $video->duration = $job['Job']['Output']['Duration'];
            $video->settings->width = $job['Job']['Output']['Width'];
            $video->settings->height = $job['Job']['Output']['Height'];
            $video->save();

            return true;
        }

        return false;
    }

    public static function getThumbnail($video)
    {
        $s3Service = App::make('\App\Services\AWS\S3Service');

        if ($video->thumbnail == null) {
            $thumbnail = $s3Service->getThumbnail($video->filename . '-700thumb-00002.png');
    } else {
            $thumbnail = $s3Service->getThumbnail($video->thumbnail);
        }

        return $thumbnail;
    }

    public static function getVideo($video)
    {
        $s3Service = App::make('\App\Services\AWS\S3Service');

        return $s3Service->getObject($video->filename);
    }

    public static function getPlayRate($video)
    {
        $plays = $video->videoPlays()->where('duration', '>', 0)->count();
        $loads = $video->videoPlays()->count();

        if ($loads > 0) {
            $play_rate = round(($plays/$loads)*100);
        } else {
            $play_rate = 0;
        }

        return $play_rate;
    }

    public static function getHoursWatched($video)
    {
        $hours_watched = round(($video->videoPlays()->sum('duration')*($video->duration/100))/3600, 2);

        return $hours_watched;
    }

    public static function formatFilesize($filesize)
    {
        $units = array(' B', ' KB', ' MB', ' GB', ' TB');

        for ($i = 0; $filesize > 1024; $i++) {
            $filesize /= 1024;
        }

        return round($filesize, 2).$units[$i];
    }

    public static function pixelbombCode($code)
    {
        $code = addslashes($code);
        $code = str_replace('<script>', '<script>', $code); 
        $code = str_replace('</script>', '<" + "/" + "script>', $code); 
        return str_replace(array(chr(10), chr(13)),' ',$code);
    }

    // Get video embed settings
    public static function getEmbedClass($video) {
        $class = '';

        if($video->settings->autoplay == true) {
            $class .= ' autoplay';
        }

        if($video->settings->playbar != true) {
            $class .= ' no-playbar';
        }

        if($video->settings->big_play != true) {
            $class .= ' no-big-play';
        }

        if($video->settings->small_play != true) {
            $class .= ' no-play-control';
        }

        if($video->settings->volume != true) {
            $class .= ' no-volume-control';
        }

        if($video->settings->fullscreen != true) {
            $class .= ' no-full-screen';
        }

        if($video->settings->progress != true) {
            $class .= ' no-progress-control';
        }

        if($video->settings->time != true) {
            $class .= ' no-time-control';
        }

        if($video->settings->display_hd != true) {
            $class .= ' no-hd';
        }

        if($video->settings->logo != true) {
            $class .= ' no-logo';
        }

        return $class;
    }

    // Get video autoplay setting
    public static function getAutoplaySetting($video) {
        $autoplay = '';

        if($video->settings->autoplay == true) {
            $autoplay = 'autoplay preload="auto"';
        }

        return $autoplay;
    }
}

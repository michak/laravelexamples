<?php namespace App\Helpers;

use App;

/**
* Helpers to easily get video informations among views
*/
class ResourcesHelper
{
    public static function storageUsage($used_storage)
    {
        $available_storage = 100 * 1073741824; //100GB

        $storage_usage = round(($used_storage / $available_storage) * 100, 2);

        return $storage_usage;
    }

    public static function transferUsage($used_transfer)
    {
        $available_transfer = 100 * 1073741824; //100GB

        $transfer_usage = round(($used_transfer / $available_transfer) * 100, 2);

        return $transfer_usage;
    }
}

<?php

namespace App\Models;

use App\Services\AWS\S3Service;
use App\Observers\VideoObserver;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['title', 'description'];

    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }

    public function settings()
    {
        return $this->hasOne('App\Models\VideoSetting');
    }

    public function videoPlays()
    {
        return $this->hasMany('App\Models\VideoPlay');
    }

    public function sales()
    {
        return $this->hasMany('App\Models\Sale');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function callActionHtml()
    {
        return $this->hasOne('App\Models\CallAction')->where('type', 'html');
    }

    public function callActionText()
    {
        return $this->hasOne('App\Models\CallAction')->where('type', 'text');
    }

    public function callActionImage()
    {
        return $this->hasOne('App\Models\CallAction')->where('type', 'image');
    }

    public function callAction()
    {
        return $this->hasOne('App\Models\CallAction')->where('enabled', true);
    }

    public function pixelbombs()
    {
        return $this->hasMany('App\Models\Pixelbomb');
    }

    public function pixelbombFacebook()
    {
        return $this->hasOne('App\Models\Pixelbomb')->where('type', 'facebook');
    }

    public function pixelbombGoogle()
    {
        return $this->hasOne('App\Models\Pixelbomb')->where('type', 'google');
    }

    public function pixelbombCustom()
    {
        return $this->hasOne('App\Models\Pixelbomb')->where('type', 'custom');
    }

    public static function boot()
    {
        parent::boot();

        self::observe(new VideoObserver());
    }

    public function getThumbnailsAttribute()
    {
        $s3Service = new S3Service;
        $iteratorThumbnails = $s3Service->getThumbnails($this->filename);

        $thumbnails = collect();

        foreach ($iteratorThumbnails as $iteratorThumbnail) {
            $thumbnail = collect(array(
                'key' => $iteratorThumbnail['Key'],
                'url' => $s3Service->getThumbnail($iteratorThumbnail['Key'])
            ));

            $thumbnails->push($thumbnail);
        }

        return $thumbnails;
    }

    public function getThumbnailAttribute($value)
    {
        if ($value == null) {
            $value = $this->filename . '-700thumb-00002.png';
        }

        return $value;
    }
}

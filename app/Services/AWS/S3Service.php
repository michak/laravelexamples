<?php namespace App\Services\AWS;

use AWS;
use Auth;

/**
* Class to handle file upload to AWS S3
*/
class S3Service
{
    protected $input_bucket;
    protected $output_bucket;
    protected $thumbs_bucket;
    
    public function __construct()
    {
        $this->input_bucket = config('aws.input_bucket');
        $this->output_bucket = config('aws.output_bucket');
        $this->thumbs_bucket = config('aws.thumbs_bucket');

        $this->s3 = AWS::createClient('s3');
    }

    public function upload($filename, $file)
    {
        $this->s3->upload(
            $this->input_bucket,
            $filename,
            file_get_contents($file),
            'public-read',
            array('params' => array(
                'CacheControl'  => 'max-age=2629000'
            ))
        );

        return true;
    }

    public function uploadThumbnail($filename, $file)
    {
        $this->s3->upload(
            $this->thumbs_bucket,
            $filename,
            file_get_contents($file),
            'public-read',
            array('params' => array(
                'CacheControl'  => 'max-age=2629000'
            ))
        );

        return true;
    }

    public function getObject($key)
    {
        $video = array();

        $video['sd'] = $this->getObjectUrl(
            $this->output_bucket,
            'sd_' . $key . '.mp4',
            '+30 minutes'
        );

        $video['hd'] = $this->getObjectUrl(
            $this->output_bucket,
            'hd_' . $key . '.mp4',
            '+30 minutes'
        );

        return $video;
    }

    public function getObjectUrl($key, $bucket)
    {
        $url = $this->s3->getObjectUrl(
            $key,
            $bucket,
            '+30 minutes'
        );

        return $url;
    }

    public function getThumbnail($key)
    {
        $url = $this->getObjectUrl(
            $this->thumbs_bucket,
            $key,
            '+30 minutes'
        );

        return $url;
    }

    public function getThumbnails($key)
    {
        $thumbnails = $this->s3->getIterator('ListObjects', array(
            'Bucket' => $this->thumbs_bucket,
            'Prefix' => $key
        ));

        return $thumbnails;
    }

    public function deleteObject($video)
    {
        $this->s3->deleteObject(array(
            'Bucket' => $this->output_bucket,
            'Key' => 'sd_' . $video->filename . '.mp4'
        ));

        $this->s3->deleteObject(array(
            'Bucket' => $this->output_bucket,
            'Key' => 'hd_' . $video->filename . '.mp4'
        ));

        $this->s3->deleteObject(array(
            'Bucket' => $this->input_bucket,
            'Key' => $video->filename
        ));

        $this->s3->deleteObject(array(
            'Bucket' => $this->thumbs_bucket,
            'Key' => "/" . $video->filename . ".*/"
        ));

        return true;
    }
}

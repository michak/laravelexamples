<?php namespace App\Services\AWS;

use AWS;
use Auth;

/**
* Class to handle file upload to AWS S3
*/
class TranscoderService
{
    protected $bucket;
    
    public function __construct()
    {
        $this->bucket = config('aws.input_bucket');
        $this->pipeline_id = config('aws.transcoder.pipeline_id');
        $this->preset_sd = config('aws.transcoder.preset_sd');
        $this->preset_hd = config('aws.transcoder.preset_hd');
    }

    public function transcode($filename)
    {
        $transcoder = AWS::createClient('elastictranscoder');

        $filename_sd = 'sd_' . $filename . '.mp4';
        $filename_hd = 'hd_' . $filename . '.mp4';
        $thumbnail = $filename;

        $job = $transcoder->createJob(array(
            'PipelineId' => $this->pipeline_id,
            'Input' => array(
                'Key' => $filename
            ),
            'Outputs' => array(
                array(
                    'Key' => $filename_sd,
                    'PresetId' => $this->preset_sd,
                    'ThumbnailPattern' => $thumbnail . '-700thumb-{count}',
                ),
                array(
                    'Key' => $filename_hd,
                    'PresetId' => $this->preset_hd
                ),
            ),
        ));

        return $job['Job']['Id'];
    }
}

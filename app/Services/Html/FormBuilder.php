<?php namespace App\Services\Html;

/**
* Overwrites and add custom form elements
*/
class FormBuilder extends \Collective\Html\FormBuilder
{
    /**
     * Overwrite a form label element.
     *
     * @param  string $name
     * @param  string $value
     * @param  array  $options
     *
     * @return string
     */
    public function label($name, $value = null, $options = [])
    {
        $this->labels[] = $name;

        $options = $this->html->attributes($options);

        $value = $this->formatLabel($name, $value);

        return '<label for="' . $name . '"' . $options . '>' . $value . '</label>';
    }
}

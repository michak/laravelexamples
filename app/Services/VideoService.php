<?php namespace App\Services;

use Image;
use Validator;
use Illuminate\Support\Str;

use App\Services\AWS\S3Service;
use App\Repositories\VideoRepository;
use App\Services\AWS\TranscoderService;

class VideoService
{

    public function __construct(VideoRepository $videoRepository, S3Service $s3Service, TranscoderService $transcoderService)
    {
        $this->destinationPath = config('app.destination_path');
        $this->videoRepository = $videoRepository;
        $this->s3Service = $s3Service;
        $this->transcoderService = $transcoderService;
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'title'       => 'required|string|max:255',
            'description' => 'string',
            'video'       => 'required|mimes:mp4,3gpp,quicktime,x-msvideo,x-ms-wmv,ogg,webm,MP2T,x-flv,x-mpegURL'
        ]);
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function callActionValidator(array $data)
    {
        return Validator::make($data, [
            'image.enabled' => 'boolean',
            'image.url' => 'string|url',
            'image.file'  => 'mimes:jpg,jpeg,png,gif',
            'text.enabled' => 'boolean',
            'text.url' => 'string|url',
            'text.content'  => 'string',
            'html.enabled' => 'boolean',
            'html.content' => 'string'
        ]);
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function thumbnailValidator(array $data)
    {
        return Validator::make($data, [
            'thumbnail'         => 'string',
            'custom_thumbnail'  => 'mimes:jpg,jpeg,png,gif'
        ]);
    }

    public function upload($request)
    {
        if ($request->hasFile('video')) {
            if ($request->file('video')->isValid()) {
                $video = $request->file('video');
                $filesize = $video->getSize();
                $filename = $this->videoRepository->getFileName($video);
                $video->move(config('aws.video_destination_path'), $filename);

                if ($this->s3Service->upload($filename, (config('aws.video_destination_path') . $filename))) {
                    unlink(config('aws.video_destination_path') . $filename);

                    if ($job_id = $this->transcoderService->transcode($filename)) {
                        return array(
                            'filename' => $filename,
                            'job_id'   => $job_id,
                            'filesize'  => $filesize
                        );
                    }
                }
            }
        }

        return false;
    }

    public function uploadThumbnail($video, $request)
    {
        if ($request->hasFile('custom_thumbnail')) {
            if ($request->file('custom_thumbnail')->isValid()) {
                $extension = $request->file('custom_thumbnail')->getClientOriginalExtension();
                $filename = $video->filename . '-' . time() . '.' . $extension;
                $thumbnail = Image::make($request->file('custom_thumbnail'))
                    ->resize($video->settings->width, $video->settings->height, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save(config('aws.video_destination_path') . $filename);

                if ($this->s3Service->uploadThumbnail($filename, (config('aws.video_destination_path') . $filename))) {
                    unlink(config('aws.video_destination_path') . $filename);

                    return $filename;
                }
            }
        }

        return false;
    }

    public function remove($video)
    {
        $this->s3Service->deleteObject($video);
    }
}

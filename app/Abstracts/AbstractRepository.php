<?php namespace App\Abstracts;

use Uuid;
use ReflectionClass;
use Illuminate\Support\Str;
use App\Repositories\RepositoryInterface;

/**
 * The Abstract Repository provides default implementations of the methods defined
 * in the base repository interface. These simply delegate static function calls
 * to the right eloquent model based on the $modelClassName.
 */

abstract class AbstractRepository implements RepositoryInterface
{
    protected $modelClassName;

    public function all($columns = array('*'))
    {
        return call_user_func_array("{$this->modelClassName}::all", array($columns));
    }

    public function create(array $attributes, $relationship = null)
    {
        return call_user_func_array("{$this->modelClassName}::create", array($attributes));
    }

    public function find($id, $columns = array('*'))
    {
        return call_user_func_array("{$this->modelClassName}::find", array($id, $columns));
    }

    public function findBySlug($slug, $authorize = false)
    {
        return call_user_func_array("{$this->modelClassName}::where", array(array('slug' => $slug)));
    }

    public function where($criteria = array())
    {
        return call_user_func_array("{$this->modelClassName}::where", array($criteria));
    }

    public function update($object, array $attributes)
    {
        return $object->update($attributes);
    }

    public function delete($id)
    {
        return call_user_func_array("{$this->modelClassName}::destroy", array($id));
    }

    public function getSlug($title)
    {
        $slug = Uuid::generate(4, $title);
        $results = call_user_func("{$this->modelClassName}::whereRaw", "slug REGEXP '^{$slug}(-[0-9]*)?$'");
        $slugCount = $results->count();
     
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

    public function createInstance($params)
    {
        $reflection_class = new ReflectionClass($this->modelClassName);
        return $reflection_class->newInstanceArgs($params);
    }

    public function replicate($object)
    {
        $clone = $object->replicate();
        $clone->slug = $this->getSlug($object->title);
        return $clone->save();
    }
}

<?php namespace App\Observers;

class VideoObserver
{
    public function created($model)
    {
        $model->settings()->create(array());

        $model->callActionHtml()->create(array('type' => 'html'));
        $model->callActionText()->create(array('type' => 'text'));
        $model->callActionImage()->create(array('type' => 'image'));
        $model->pixelbombFacebook()->create(array('type' => 'facebook'));
        $model->pixelbombGoogle()->create(array('type' => 'google'));
        $model->pixelbombCustom()->create(array('type' => 'custom'));
    }
}
